# Calculadora de IMC com React native e ExpoGO

Para realizar a instalação das dependências do Projeto, navega até a pasta de conteúdo da aplicação e rode o seguinte comando abaixo:

```
npm install
``` 

Aguarde a instalação das dependências e logo em seguida rode o projeto com o seguinte comando:

```
npm start
```

Have fun! Projeto criado para uma atividade acadêmica do IMD, 2023